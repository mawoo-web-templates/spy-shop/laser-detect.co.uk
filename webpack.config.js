const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin"); // ExtractTextPlugin
const HtmlWebpackPlugin = require('html-webpack-plugin'); // HtmlWebpackPlugin
const CopyWebpackPlugin = require('copy-webpack-plugin'); // Copy Webpack Plugin
const UglifyJsPlugin = require('uglifyjs-webpack-plugin') // UglifyjsWebpackPlugin

module.exports = {
    entry: './src/index.js',
    output: {
        filename: 'js/app.js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [{
            test: /\.scss$/,
            use: ExtractTextPlugin.extract({
                use: [{
                    loader: 'css-loader',
                    options: {
                        minimize: true,
                        sourceMap: true
                    }
                }, {
                    loader: 'postcss-loader',
                    options: {
                        sourceMap: true
                    }
                }, {
                    loader: 'sass-loader',
                    options: {
                        sourceMap: true
                    }
                }],
                // use style-loader in development
                fallback: "style-loader"
            })
        }, {
            test: /\.(gif|png|jpe?g|svg)$/i,
            use: [{
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    outputPath: 'img/'
                }
            }, {
                loader: 'image-webpack-loader',
                options: {
                    bypassOnDebug: true,
                }
            }]
        }]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: "css/[name].[contenthash:8].css"
        }),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: './src/tpl/index.tpl.html',
            title: 'Professional threats detection - detect explosives and drugs',
            description: 'Detecting station explosives and drugs intended for LDS control points.',
            minify: {
                collapseWhitespace: true,
                minifyCSS: true,
                minifyJS: true,
                removeComments: true
            }
        }),
        new HtmlWebpackPlugin({
            filename: 'applications-lds.html',
            template: './src/tpl/base.tpl.html',
            templatePage: './pages/applications-lds.html',
            title: 'Professional detectors from LDS designed for detection of threats',
            description: 'Detection of explosives, drugs, and chemicals with professional equipment  LDS  presented at  Detective Store.',
            breadcrumbTitle: '',
            minify: {
                collapseWhitespace: true,
                minifyCSS: true,
                minifyJS: true,
                removeComments: true
            }
        }),
        new HtmlWebpackPlugin({
            filename: 'b-scan-8500-b-explosive-materials-and-drugs-detection-station.html',
            template: './src/tpl/base.tpl.html',
            templatePage: './pages/b-scan-8500-b.html',
            title: 'B-Scan LDS 5500-B - station, which is able to detect explosives',
            description: 'Station for detecting explosives and drugs intended for LDS checkpoints.',
            breadcrumbTitle: '',
            minify: {
                collapseWhitespace: true,
                minifyCSS: true,
                minifyJS: true,
                removeComments: true
            }
        }),
        new HtmlWebpackPlugin({
            filename: 'g-scan-pro-4500-g-i-blasting-materials-and-drugs-detector.html',
            template: './src/tpl/base.tpl.html',
            templatePage: './pages/g-scan-pro-4500-g.html',
            title: 'G-SCAN 4500-G - drug detection',
            description: 'Manual detector of explosives and drugs from LDS.',
            breadcrumbTitle: '',
            minify: {
                collapseWhitespace: true,
                minifyCSS: true,
                minifyJS: true,
                removeComments: true
            }
        }),
        new HtmlWebpackPlugin({
            filename: 'i-scan-lds-3500-i-blasting-materials-detector.html',
            template: './src/tpl/base.tpl.html',
            templatePage: './pages/i-scan-lds-3500-i.html',
            title: 'I-SCAN LDS 3500-i - detector of explosives',
            description: 'Manual detector of fumes and traces produced by LDS.',
            breadcrumbTitle: '',
            minify: {
                collapseWhitespace: true,
                minifyCSS: true,
                minifyJS: true,
                removeComments: true
            }
        }),
        new HtmlWebpackPlugin({
            filename: 'lds-technology.html',
            template: './src/tpl/base.tpl.html',
            templatePage: './pages/lds-technology.html',
            title: 'Technology created by LDS',
            description: 'Raman spectroscopy is just one of the LDS technologies that allows you to quickly and efficiently test for drugs or explosives in any place.',
            breadcrumbTitle: '',
            minify: {
                collapseWhitespace: true,
                minifyCSS: true,
                minifyJS: true,
                removeComments: true
            }
        }),
        new HtmlWebpackPlugin({
            filename: 'security-rules-and-policy.html',
            template: './src/tpl/base.tpl.html',
            templatePage: './pages/security-rules-and-policy.html',
            title: 'Regulations and website privacy policy',
            description: 'Regulations and website privacy policy.',
            breadcrumbTitle: '',
            minify: {
                collapseWhitespace: true,
                minifyCSS: true,
                minifyJS: true,
                removeComments: true
            }
        }),
        new HtmlWebpackPlugin({
            filename: 'threat-detection-with-laser-detect-systems.html',
            template: './src/tpl/base.tpl.html',
            templatePage: './pages/threat-detection-with-laser-detect-systems.html',
            title: 'Laser Detect Systems',
            description: 'LDS is a company specializing in detecting threats using modern technology, which is used by the army, police and services around the world.',
            breadcrumbTitle: '',
            minify: {
                collapseWhitespace: true,
                minifyCSS: true,
                minifyJS: true,
                removeComments: true
            }
        }),
        new CopyWebpackPlugin([{
            from: './src/googlecf68af412099da49.html'
        }, {
            from: './src/.htaccess'
        }, {
            from: './src/favicon-16x16.png'
        }, {
            from: './src/favicon-32x32.png'
        }, {
            from: './src/phpmailer',
            to: 'phpmailer'
        }]),
        new UglifyJsPlugin({
            sourceMap: true
        })
    ],
    devServer: {
        contentBase: path.join(__dirname, "dist"),
        compress: true,
        port: 9000
    },
    devtool: 'source-map',
    watch: true
};