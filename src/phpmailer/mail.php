<?php

// Form
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (!empty($_POST["name"])) {
        $name = test_input($_POST["name"]);
    }

    if (!empty($_POST["email"])) {
        $email = test_input($_POST["email"]);
    }

    if (!empty($_POST["phone"])) {
        $phone = test_input($_POST["phone"]);
    }

    if (!empty($_POST["message"])) {
        $message = test_input($_POST["message"]);
    }
}

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

// reCAPTCHA widget
$userIP            = $_SERVER["REMOTE_ADDR"];
$gRecaptchaResponse = $_POST['g-recaptcha-response'];
$secretKey         = '';
$request           = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secretKey}&response={$gRecaptchaResponse}&remoteip={$userIP}");

if (!strstr($request, "true")) {
    echo 'grecaptcha-false';
} else {
    require 'PHPMailerAutoload.php';

    $mail = new PHPMailer;

    // Enable verbose debug output
    //$mail->SMTPDebug = 3;

    // Set mailer to use SMTP
    $mail->isSMTP();

    // Specify main and backup SMTP servers
    $mail->Host       = '';

    // Enable SMTP authentication
    $mail->SMTPAuth   = true;

    // SMTP username
    $mail->Username   = '';

    // SMTP password
    $mail->Password   = '';

    // Enable TLS encryption, `ssl` also accepted
    $mail->SMTPSecure = 'tls';

    // TCP port to connect to
    $mail->Port       = 587;

    $mail->setFrom($email, $name);
    $mail->addAddress('', '');

    $mail->isHTML(true); // Set email format to HTML
    $mail->CharSet = 'UTF-8'; // The character set of the message.

    $mail->Subject = 'Wiadomość ze strony laser-detect.pl';
    $mail->Body    = "<p>" . "<b>Imię i Nazwisko</b>: " . $name . "<br><b>Numer telefonu</b>: " . $phone . "<br><b>Treść wiadomości</b>: " . $message . "</p>";
$mail->AltBody = "Imię i Nazwisko: " . $name . "\n Numer telefonu: " . $phone . "\n Treść wiadomości: " . $message;

    if (!$mail->send()) {
        echo 'phpmailer-error';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
        echo 'phpmailer-success';
    }
}