// Form
(function ($) {
    var form = {
        add: function () {
            var html = '',
                field = [{
                    id: 'name',
                    label: 'Name and Surname:',
                    tag: 'input',
                    type: 'text'
                }, {
                    id: 'email',
                    label: 'E-mail address:',
                    tag: 'input',
                    type: 'email'
                }, {
                    id: 'phone',
                    label: 'Phone:',
                    tag: 'input',
                    type: 'tel'
                }, {
                    id: 'message',
                    label: 'Message:',
                    tag: 'textarea',
                    type: ''
                }];

            html += '<form class="contact-form">';

            for (var i = 0; i < field.length; i++) {
                var fieldId = field[i].id,
                    fieldLabel = field[i].label,
                    fieldType = (field[i].type !== '') ? 'type="' + field[i].type + '"' : '',
                    fieldTag = field[i].tag;

                html += '<div class="form-group">';
                html += '<' + fieldTag + ' id="' + fieldId + '" class="form-control" placeholder="' + fieldLabel + '" name="' + fieldId + '" required ' + fieldType + '>' + ((field[i].tag !== 'input') ? '</' + field[i].tag + '>' : '');
                html += '</div>';
            }

            html += '<div class="row">';
            html += '<div class="col-sm-8">';
            html += '<div id="grecaptcha" class="grecaptcha"></div>';
            html += '</div>';
            html += '<div class="col-sm-4">';
            html += '<p class="submit"><button type="submit" class="btn btn-primary btn-lg">Send</button></p>';
            html += '</div>';
            html += '</div>';
            html += '<div id="notice" class="notice"></div>';
            html += '</form>';

            $('#contact-form').append(html);
        },
        status: function (msg, formObj) {
            if (msg === 'phpmailer-success') {
                $('#notice').html('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Thank you for taking the time to contact us, your message was successfully sent!</div>').hide().slideDown();
                // Form and reCaptcha reset
                grecaptcha.reset();
                formObj[0].reset();
            } else if (msg === 'phpmailer-error') {
                $('#notice').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Your message has not been sent. Please try again later.</div>').hide().slideDown();
            } else if (msg === 'grecaptcha-false') {
                $('#notice').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Please verify that you are not a robot!</div>').hide().slideDown();
            }
        },
        validation: function (formObj) {
            this.xhr(formObj);
        },
        xhr: function (formObj) {
            $.ajax({
                    url: 'phpmailer/mail.php',
                    type: 'POST',
                    data: formObj.serialize()
                })
                .done(function (response) {
                    form.status(response, formObj);
                })
                .fail(function (response) {
                    form.status(response);
                });
        }
    }

    form.add();

    $('body').on('submit', '#contact-form > form', function (event) {
        event.preventDefault();
        form.validation($(this));
    });
}(jQuery));